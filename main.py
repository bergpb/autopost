import time

# from schedule import every, repeat, run_pending

import autopost
from db import Database
from ext.logger import logger
from ext.config import Config

config = Config()

db = Database(config.ENV, config.MONGO_URL, config.MONGO_USER, config.MONGO_PASSWORD, config.MONGO_PORT)

logger.debug(f"Environment: {config.ENV}, Log Level: {config.LOG_LEVEL}")

# Using CronJobs
data = autopost.get_data(config.GITHUB_API_TOKEN, config.PER_PAGE)
autopost.send_message(db, config.TELEGRAM_BOT_TOKEN, config.TELEGRAM_CHAT_ID, data)

# Using Python Scheduler
# @repeat(every().day.at(config.TIME))
# def scheduled_tasks():
#     """Task to run everyday to send message to Telegram channel"""
#     data = autopost.get_data(config.GITHUB_API_TOKEN, config.PER_PAGE)
#     autopost.send_message(db, config.TELEGRAM_BOT_TOKEN, config.TELEGRAM_CHAT_ID, data)
#     pass


# while True:
#     run_pending()
#     time.sleep(1)
