# Autopost

Send stared Github repositories in my personal account to Telegram channel

This application works with docker-compose or kubernetes
I've used docker-compose for development environment and Kubernetes to prod environment

Images are generated automatically using Gitlab CI workflow, when a new tag is generated, a new container image is pushed to Gitlab Registry.

### Development
[work in progress]
```
poetry install # install packages with poetry
poetry shell # init virtualenvironment with poetry
poetry run # start application
```

### Kubernetes deployment (using kustomize):

Good article explaining how kustomize works: [Kustomize — Kubernetes native configuration management](https://subbaramireddyk.medium.com/kustomize-kubernetes-native-configuration-management-f51630d29ac0)

About namespaces in kustomize: https://stackoverflow.com/a/75402083/6539270

```
kustomize build overlays/<dev|prod> | kubectl apply -f .
or
kubectl apply -k overlays/<dev|prod>
```
