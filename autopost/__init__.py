from datetime import datetime as dt
from math import ceil
from time import sleep

from ext.utils import format_message

from httpx import get, post

from ext.logger import logger

from typing import Type

__version__ = "0.1.0"


def get_data(api_token: str, per_page: int) -> list:
    """Get data ffrom Github API

    Args:
        api_token (str): Github API token
        per_page (int): Number of results that will be returned in Github API

    Returns:
        data (list): List with newest repositories that received a star in Github
    """
    data = []

    logger.info(f"Checking if have new data to be sent...")
    logger.info(f"Getting information from Github API...")

    try:
        res = get(
            f"https://api.github.com/user/starred?per_page={per_page}",
            headers = {
                "Accept": "application/vnd.github+json",
                "Authorization": f"token {api_token}",
            },
        )

        if res.status_code == 200:
            for item in res.json():
                data.append(
                    {
                        "name": item["name"],
                        "description": item["description"],
                        "html_url": item["html_url"],
                        "stargazers_count": item["stargazers_count"],
                        "language": item["language"],
                        "topics": item["topics"],
                        "created_at": dt.now(),
                    }
                )

            return data
        else:
            logger.error(f"Failed to get Github API info: {res.status_code}")
    except Exception as e:
        logger.exception(f"Failed to get data from Github API: {e}")


def send_message(db, telegram_api_token: str, chat_id: str, data: list) -> None:
    """Send new repositories to Telegram channel

    Args:
        db (Database): Database object to interact with database
        telegram_api_token (str): Token to connect with Telegram API
        chat_id (str): Telegram Chat ID to identify the channel to send messages
        data (str): List with newest repositories that received a star in Github
    """
    for item in data:
        search = db.find_one("name", item["name"])

        if search is None:
            logger.info(f"Sending repository to the channel: {item['name']}")
            message = format_message(item)

            try:
                res = post(
                    f"https://api.telegram.org/bot{telegram_api_token}/sendMessage",
                    data = {
                        "parse_mode": "MarkdownV2",
                        "chat_id": f"{chat_id}",
                        "text": f"{message}",
                    },
                )

                if res.status_code == 200:
                    db.insert(item)
                    logger.debug(f"Repository inserted: {item['name']}")
                elif res.status_code == 429:
                    error_data = res.json()
                    sleep_time = ceil(int(res.headers["retry-after"]) / 10) * 10
                    logger.warning(
                        f"Fail to send message: {res.status_code} {error_data['description']}, waiting {sleep_time} seconds..."
                    )
                    sleep(sleep_time)
                else:
                    error_data = res.json()
                    logger.critical(
                        f"Failed to send message: {message} {res.status_code} {error_data['description']}"
                    )
                    res.raise_for_status()
            except Exception as e:
                logger.exception(f"Failed to send message to channel: {e}")
