FROM python:3.9-alpine3.16 AS base

RUN mkdir -p /home/app

WORKDIR /home/app


FROM base as build

RUN apk update && apk add curl

COPY requirements.txt requirements.txt

RUN python -m venv .venv && \
    .venv/bin/pip install --upgrade pip wheel && \
    .venv/bin/pip install -r requirements.txt

FROM base as runtime

ENV PATH=/home/app/.venv/bin:$PATH

COPY --from=build /home/app/.venv /home/app/.venv

COPY . /home/app/

CMD ["python", "main.py"]
