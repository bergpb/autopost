import pymongo


class Database(object):
    def __init__(self, env: str, url: str, user: str, password: str, port: str):
        driver = "mongodb://" if (env == "dev") else "mongodb+srv://"
        opts = f":{port}/" if (env == "dev") else f"/?retryWrites=true&w=majority"

        self.conn = pymongo.MongoClient(f"{driver}{user}:{password}@{url}{opts}")
        self.db = self.conn["autopost"]
        self.col = self.db["repositories"]

    def insert(self, data: dict):
        return self.col.insert_one(data)

    def find_one(self, field: str, value: str):
        return self.col.find_one({f"{field}": f"{value}"})

    def find(self, field: str, value: str):
        return self.col.find_one({f"{field}": f"{value}"})
