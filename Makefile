NAME      := registry.gitlab.com/bergpb/autopost
IMG       := ${NAME}:$$(git describe --tags $$(git rev-list --tags --max-count=1))
LATEST    := ${NAME}:latest

run-infra:
	@docker compose -f docker-compose-dev.yml up -d

stop-infra:
	@docker compose -f docker-compose-dev.yml down

build:
	@docker build -t ${IMG} .
	@docker tag ${IMG} ${LATEST}

push:
	@docker push ${IMG}
	@docker push ${LATEST}

multi-arch:
	@docker buildx build --push \
	--platform linux/amd64,linux/arm64,linux/arm/v7,linux/arm/v6 \
	--tag ${IMG} .

deploy:
	@git pull
	@docker-compose up -d --no-deps --build

format:
	@isort .
	@black .
