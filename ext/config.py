from os import getenv

from dotenv import load_dotenv

load_dotenv()


class Config:
    ENV = getenv("ENV")

    LOG_LEVEL = getenv("LOG_LEVEL")

    MONGO_URL = getenv("MONGO_URL")
    MONGO_USER = getenv("MONGO_USER")
    MONGO_PASSWORD = getenv("MONGO_PASSWORD")
    MONGO_PORT = getenv("MONGO_PORT", "")

    GITHUB_API_TOKEN = getenv("GITHUB_API_TOKEN")
    PER_PAGE = int(getenv("PER_PAGE"))

    TELEGRAM_BOT_TOKEN = getenv("TELEGRAM_BOT_TOKEN")
    TELEGRAM_CHAT_ID = getenv("TELEGRAM_CHAT_ID")
