import sys
from os import getenv

from loguru import logger

from ext.config import Config

config = Config()

try:
    level = config.LOGGER_LEVEL
except AttributeError:
    level = "INFO"

formatter = (
    "[{level.icon}{level:^10}] {time:YYYY-MM-DD hh:mm:ss} {file} - {name}: {message}"
)

logger.remove()  # https://github.com/Delgan/loguru/issues/208
logger.add(sys.stderr, level=config.LOG_LEVEL, colorize=True)
