def escape_characters(data: str) -> str:
    if data is not None:
        return data.translate(
            str.maketrans(
                {
                    "-": r"\-",
                    "_": r"\_",
                    "]": r"\]",
                    "[": r"\[",
                    "(": r"\(",
                    ")": r"\)",
                    "!": r"\!",
                    "*": r"\*",
                    ".": r"\.",
                    "#": r"\#",
                    "+": r"\+",
                    "~": r"\~",
                    "=": r"\=",
                    "|": r"\|",
                    "{": r"\{",
                    "}": r"\}",
                    ">": r"\>",
                    "<": r"\<",
                }
            )
        )


def format_message(data: str) -> str:
    """Format message before send to channel

    Args:
        data (str): Github API data to be formated

    Returns:
        str: Message that will be send to channel
    """
    description = escape_characters(data["description"])
    stagazers = data["stargazers_count"]
    language = escape_characters(data["language"])
    url_name = escape_characters(data["name"])
    url_link = data["html_url"]
    topics_list = [f"\#{item}".replace("-", "\_") for item in data["topics"]]
    topics = " ".join(topics_list)

    message = f"""
*📰 Description:* {description}
*🔗 URL:* [{url_name}]({url_link})
*🌟 Stars:* {stagazers}
*🌎 Language:* {language if language else '🚫'}
*📍 Topics:* {topics if topics else '🚫'}"""

    return message
